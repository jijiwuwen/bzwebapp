# -*- coding:utf-8 -*-

import sqlalchemy
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer,String,BigInteger,Float

# 创建基类
Base = sqlalchemy.ext.declarative.declarative_base()

# 系统参数表
class Bwpubparm(Base):
    __tablename__ = 'bwpubparm'

    id = Column(Integer,primary_key=True)
    ppid = Column(String(50))
    ppname = Column(String(100))
    ppvalue = Column(String(300))
    memo = Column(String(100))

    def __str__(self):
        return "<bwpuparm(id={},ppid={},ppname={},ppvalue={},memo={}>".format(
            self.id,self.ppid,self.ppname,self.ppvalue,self.memo
        )
    def __repr__(self):
        return self.__str__()

if __name__ == '__main__':
    from sqlalchemy import create_engine
    engine = create_engine('sqlite:///../db/bzweb.db')
    Base.metadata.create_all(engine)
    DbSession = sessionmaker(engine)
    session = DbSession()

    pparms = session.query(Bwpubparm).filter(Bwpubparm.ppid == 'sys_url').all()
    print(len(pparms))
    if len(pparms) > 0:
        print(pparms[0].ppvalue)
    else:
        pparm = Bwpubparm(ppid='sys_url',ppname='系统应用地址',ppvalue='http://localhost/bzcarapi/public/index.php',memo='')
        session.add(pparm)
        session.commit()


    session.close()
    print('over')
